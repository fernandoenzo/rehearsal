# ACME CERTIFICATIONS, INC.

## Requisitos previos:


1. Eclipse Indigo IDE for Java EE Developers + Maven Integration for Eclipse WTP (m2e-wtp).  
   
2. MySQL Server 5.5  
   
3. Apache Tomcat 7.0  
   
4. Si se está utilizando Linux, seguir el siguiente [manual][1] desde el punto 3 hasta el punto 13 (ambos incluídos) para configurar correctamente Tomcat y Eclipse.  
   
--------------------------------------------------------

## Configuración del proyecto:

1. Tras descargar el proyecto de Git y asegurarnos de que está ubicado en el workspace, renombrar la carpeta a «ACME», abrir Eclipse e importar el proyecto. El proceso puede llevar varios minutos y puede incluso saltar algún error durante el mismo, pero no hay que hacer absolutamente nada salvo esperar.  
   
2. Una vez el proyecto esté importado al 100%, es muy probable que aparezcan decenas de errores en el mismo y que hayamos ralentizado bastante nuestra máquina (virtual o no) al añadirlo a nuestro workspace en Eclipse, por lo que se recomienda cerrar Eclipse, apagar la máquina virtual (si procede), liberar la memoria caché del ordenador si se está utilizando Linux como sistema operativo (bien utilizando el Terminal, bien reiniciando el ordenador) y finalmente reabrir Eclipse.  
   
3. En la ventana de Eclipse, seleccionar Project → Clean y limpiar nuestro proyecto. Deberían desaparecer algunos errores, aunque no todos.  
   
4. Click derecho sobre el proyecto → Maven → Update project...  
Marcar la casilla de "Force Update of Snapshots/Releases" y aceptar. Esto puede dar un error de JPA pero no hay de qué preocuparse, aceptamos y listo.  
   
5. Click derecho sobre el proyecto → Properties → Project Facets y marcar las casillas "Dynamic Web Module" y "Javascript".  
Veremos que aparece una barra inferior donde pone "Further Configuration". Pinchamos ahí y donde dice "Content Directory" escribimos: `src/main/webapp`  
La casilla que habla sobre el archivo web.xml debe permanecer desactivada. Acto seguido, pulsamos OK.  
   
6. Click derecho sobre el proyecto → Maven → Update project... → OK.  
A diferencia del paso 4, aquí ya no deberíamos obtener error alguno al ejecutar el update.  
   
7. Click derecho sobre el proyecto → Properties → JPA y lo configuramos tal y como vemos en la siguiente captura:  
   
![JPA][2]  
   
Tras esto, añadiremos una nueva conexión con la base de datos.  Para ello deberemos:  
   
   • Pulsar en Add Connection  
   
   • Seleccionar la opción MySQL, darle un nombre a la conexión (ej: Acme) y pulsar Next.  
   
   • Seleccionar el MySQL JDBC Driver 5.1 con el JAR mysql-connector-java ubicado en `/usr/share/java` si estamos usando Linux.  
     `Nota:` Si el JAR no aparece en esa ubicación, instalar el paquete libmysql-java desde los repositorios de la distribución en uso.  
   
   • Finalmente, configurar los parámetros de ubicación y login de la base de datos de modo que quede algo parecido a la siguiente captura, y finalizamos:  
   
![MySQL][3]  
   
8. Añadir el proyecto a la lista de recursos del servidor Tomcat que hayamos configurado. Asegurarnos de que el servidor corre bajo el puerto 8080.  
   
9. Click derecho sobre el proyecto → JPA Tools → Generate tables from entities.  
Si se muestra por consola algún error de hibernate (como probablemente ocurra), repetir el proceso una segunda vez.  
   
10. Ejecutar el archivo `src/main/java/utilities/PopulateDatabase.java`  
   
11. Arrancar el servidor Tomcat. Nuestra aplicación estará accesible en: `http://localhost:8080/ACME`  
   
--------------------------------------------------------

## Ficheros de configuración:

En principio no es necesario tocar los siguientes archivos ya que ya están configurados. Sin embargo, no está de más conocer su ubicación por si fuera necesario modificar algo:

- #### pom.xml
Podemos tocar los contenidos de `<groupId>`, `<artifactId>`, `<name>` y `<description>`.  
El resto mejor ni mirarlo.  
   
- #### src/main/java/META-INF/persistence.xml
Cambiar el nombre de la `<persistence-unit>` si procede.  
   
- #### src/main/webapp/WEB-INF/web.xml
De aquí solo nos interesa la etiqueta `<display-name>` .  
   
- #### src/main/webapp/WEB-INF/config/data.xml
Cambiar el contenido de los tags `<jdbcUrl>` y `<persistenceUnit>` cuando sea preciso.


[1]: http://lackovic.wordpress.com/2012/05/31/set-up-eclipse-and-tomcat-7-on-ubuntu-12-04-to-create-java-restful-web-services-with-jersey/
[2]: http://img201.imageshack.us/img201/4885/jpat.png
[3]: http://img69.imageshack.us/img69/7804/mysqlz.png
