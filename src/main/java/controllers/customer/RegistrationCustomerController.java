package controllers.customer;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.CustomerService;
import services.RegistrationService;
import controllers.AbstractController;
import domain.Registration;

@Controller
@Transactional
@RequestMapping("/registration")
public class RegistrationCustomerController extends AbstractController {
	
	@Autowired
	CustomerService customerService;
	@Autowired
	RegistrationService registrationService;
	
	public RegistrationCustomerController() {
		super();
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView result;
		Collection<Registration> registrations;

		registrations = registrationService.findByPrincipal();
		result = new ModelAndView("registration/list");
		result.addObject("registrations", registrations);
		result.addObject("requestURI", "registration/list.do");

		return result;
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView result = new ModelAndView("registration/create");
		Registration registration = registrationService.create();
		result.addObject("registration", registration);

		return result;
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid @ModelAttribute Registration registration, BindingResult binding) {
		ModelAndView result;
		if (binding.hasErrors()){
			result = createEditModelAndView(registration);
		} else {
			try {
				beginTransaction();
				registrationService.registerPrincipal(registration);
				commitTransaction();
				result = new ModelAndView("redirect:list.do");
			} catch (Throwable oops){
				rollbackTransaction();
				result = createEditModelAndView(registration, "registration.commit.error");
			}
		}
		return result;
	}
	
	// Ancillary methods ------------------------------------------------------
	
		protected ModelAndView createEditModelAndView(Registration registration) {
			ModelAndView result;

			result = createEditModelAndView(registration, null);
			
			return result;
		}	
		
		protected ModelAndView createEditModelAndView(Registration registration, String message) {
			ModelAndView result;
					
			result = new ModelAndView("registration/create");
			result.addObject("registration", registration);
			result.addObject("message", message);
			
			return result;
		}

}
