package controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/welcome")
public class WelcomeController extends AbstractController {
	
	public WelcomeController() {
		super();
	}
		
	
	@RequestMapping(value = "/index")
	public ModelAndView index() {
		ModelAndView result;
		
		result = new ModelAndView("welcome/index");
		
		return result;
	}
}