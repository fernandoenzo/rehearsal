package repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Registration;

@Repository
public interface RegistrationRepository extends JpaRepository<Registration, Integer> {
	
	@Query("Select r FROM Registration r WHERE (r.owner.id = ?1) and (r.announcement = ?2)")
	Registration findByCustomerAndAnnouncement(int customerId, String announcement);

}