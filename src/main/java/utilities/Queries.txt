1. Select cus.name FROM Customer cus join cus.registrations reg WHERE reg.payment.fee > 100

2. Select cus.name as Name, SUM(reg.payment.fee) as Fee FROM Customer cus join cus.registrations reg WHERE Fee > 100 GROUP BY Name

3. Select cus.name as Name, SUM(reg.payment.fee) as Fee FROM Customer cus join cus.registrations reg GROUP BY Name ORDER BY Fee

4. SELECT SUM(p.fee) FROM Payment p WHERE p.registration.moment = '2012-01-01'

5. SELECT SUM(p.fee) FROM Payment p