package services;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import repositories.CustomerRepository;
import security.LoginService;
import security.UserAccount;
import domain.Customer;

@Service
@Transactional
public class CustomerService {

	@Autowired
	CustomerRepository customerRepository;
	@Autowired
	RegistrationService registrationService;

	public CustomerService() {
		super();
	}

	// Simple CRUD methods ----------------------------------------------------

	public Customer create() {
		Customer result;
		result = new Customer();
		return result;
	}

	public Collection<Customer> findAll() {
		Collection<Customer> result;
		result = customerRepository.findAll();
		return result;
	}

	public Customer findOne(int customerId) {
		Customer result;
		result = customerRepository.findOne(customerId);
		return result;
	}

	public void save(Customer customer) {
		assert customer != null;
		customerRepository.save(customer);
	}

	public void delete(Customer customer) {
		assert customer != null;
		customerRepository.delete(customer);
	}

	// Other business methods -------------------------------------------------

	/**
	 * Obtain the customer who is currently logged into the system.
	 */
	public Customer findByPrincipal() {
		Customer result;
		UserAccount userAccount;

		userAccount = LoginService.getPrincipal();
		assert userAccount != null;
		result = findByUserAccount(userAccount);
		assert result != null;

		return result;
	}

	/**
	 * Given an user account, obtain the Customer who is associated to it.
	 */
	public Customer findByUserAccount(UserAccount userAccount) {
		assert userAccount != null;

		Customer result;
		result = customerRepository.findByUserAccountId(userAccount.getId());
		assert result != null;

		return result;
	}

}