package services;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import repositories.ActorRepository;
import security.UserAccount;
import domain.Actor;

@Service
@Transactional
public class ActorService {
	
	@Autowired
	private ActorRepository actorRepository;

	public ActorService() {
		super();
	}
	
	// Simple CRUD methods ----------------------------------------------------
	
	public Collection<Actor> findAll() {
		Collection<Actor> result;
		
		result = actorRepository.findAll();
		
		return result;
	}

	public Actor findOne(int actorId) {
		Actor result;
		
		result = actorRepository.findOne(actorId);

		return result;
	}
	
	public void save(Actor actor) {
		assert actor != null;
		actorRepository.save(actor);
	}	
	
	public void delete(Actor actor) {
		assert actor != null;
		actorRepository.delete(actor);
	}

	// Other business methods -------------------------------------------------

	public UserAccount findUserAccount(Actor actor) {
		assert actor != null;
		return actor.getUserAccount();
	}
}