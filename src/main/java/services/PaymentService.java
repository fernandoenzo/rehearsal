package services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import repositories.PaymentRepository;
import domain.Payment;

@Service
@Transactional
public class PaymentService {
	
	@Autowired
	PaymentRepository paymentRepository;

	public PaymentService() {

	}
	
	// Simple CRUD methods ----------------------------------------------------
	
	public void save(Payment payment) {
		assert payment != null;
		paymentRepository.save(payment);
	}

	public void delete(Payment payment) {
		assert payment != null;
		paymentRepository.delete(payment);
	}
	
}