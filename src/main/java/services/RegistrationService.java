package services;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.RegistrationRepository;
import domain.Customer;
import domain.Registration;

@Service
@Transactional
public class RegistrationService {

	@Autowired
	RegistrationRepository registrationRepository;
	@Autowired
	CustomerService customerService;

	public RegistrationService() {
		super();
	}

	// Simple CRUD methods ----------------------------------------------------

	/**
	 * Creates a Registration of the logged user and an empty payment.
	 */
	public Registration create() {
		Customer logged = customerService.findByPrincipal();
		assert logged != null;

		Registration result = new Registration();
		result.setMoment(Calendar.getInstance().getTime());
		result.setOwner(logged);
		logged.addRegistration(result);

		return result;
	}

	public void save(Registration registration) {
		assert registration != null;
		registrationRepository.save(registration);
	}

	public void delete(Registration registration) {
		assert registration != null;
		registrationRepository.delete(registration);
	}

	// Business methods -------------------------------------------------------
	/**
	 * Retrieves all the registrations of the current logged customer
	 */
	public Collection<Registration> findByPrincipal() {
		Collection<Registration> result;
		Customer logged = customerService.findByPrincipal();
		result = logged.getRegistrations();
		return result;
	}

	/**
	 * Registers the logged user to a given announcement.
	 */
	public void registerPrincipal(Registration registration) {

		int announcementId = new Integer(registration.getAnnouncement()
				.replaceAll("Announcement ", ""));
		Date paymentMoment = registration.getPayment().getMoment();
		Double fee = registration.getPayment().getFee();

		Assert.isTrue(announcementId > 0 && announcementId <= 4);
		Assert.notNull(paymentMoment);
		Assert.notNull(fee);
//		Registration result = this.findByCustomerAndAnnouncement(
//				customerService.findByPrincipal(),
//				registration.getAnnouncement());
//		Assert.isNull(result);
		int comparar = registration.getMoment().compareTo(paymentMoment);
		Assert.isTrue(comparar > 0);

		this.save(registration);
	}

	/**
	 * If a given customer is registered in a given announcement, this method
	 * retrieves the registration that connects them.
	 */
	public Registration findByCustomerAndAnnouncement(Customer customer,
			String announcement) {
		assert customer != null;

		Registration result;
		result = registrationRepository.findByCustomerAndAnnouncement(
				customer.getId(), announcement);
		return result;
	}

}