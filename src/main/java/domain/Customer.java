package domain;

import java.util.Collection;
import java.util.HashSet;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Access(AccessType.PROPERTY)
public class Customer extends Actor {

	private Collection<Registration> registrations;
	private String code;

	public Customer() {
		super();

		registrations = new HashSet<Registration>();
	}

	@NotBlank
	@Pattern(regexp = "^[A-Z]-\\d+$")
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@NotNull
	@OneToMany(mappedBy = "owner")
	public Collection<Registration> getRegistrations() {
		return registrations;
	}

	public void setRegistrations(Collection<Registration> registrations) {
		this.registrations = registrations;
	}

	public void addRegistration(Registration registration) {
		registrations.add(registration);
		registration.setOwner(this);
	}

	public void removeRegistration(Registration registration) {
		registrations.remove(registration);
		registration.setOwner(null);
	}

}