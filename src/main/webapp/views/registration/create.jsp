<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<form:form action="registration/create.do" modelAttribute="registration">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="owner" />

	<form:label path="announcement">
		<spring:message code="registration.announcement" var="announcement" />
		<spring:message code="registration.announcement" />:
	</form:label>
	<form:select id="announcement" path="announcement">
		<form:option label="${announcement} 1" value="Announcement 1" />
		<form:option label="${announcement} 2" value="Announcement 2" />
		<form:option label="${announcement} 3" value="Announcement 3" />
		<form:option label="${announcement} 4" value="Announcement 4" />
	</form:select>
	<form:errors cssClass="error" path="announcement" />
	<br />

	<form:label path="moment">
		<spring:message code="registration.current.moment" />:
	</form:label>
	<form:input path="moment" readonly="true" />
	<form:errors cssClass="error" path="moment" />
	<br />
	
	<form:label path="payment.moment">
		<spring:message code="registration.payment.moment" />:
	</form:label>
	<form:input path="payment.moment" readonly="false" />
	<form:errors cssClass="error" path="payment.moment" />
	<br />

	<form:label path="payment.fee">
		<spring:message code="registration.fee" />:
	</form:label>
	<form:input path="payment.fee" />
	<form:errors cssClass="error" path="payment.fee" />
	<br />
	
	<br />
	<input type="submit" name="save" value="<spring:message code="registration.save" />" />&nbsp; 
	
	<input type="button" name="cancel"
		value="<spring:message code="registration.cancel" />"
		onclick="javascript: window.location.replace('registration/list.do')" />
	<br />

</form:form>