<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<display:table name="registrations" pagesize="3" class="displaytag"
	requestURI="${requestURI}" id="row">

	<spring:message code="registration.announcement" var="announcementHeader" />
	<display:column property="announcement" title="${announcementHeader}" sortable="true" />

	<spring:message code="registration.moment" var="momentHeader" />
	<display:column property="moment" title="${momentHeader}" sortable="true" format="{0,date,dd/MM/yyyy HH:mm}" />
	
	<spring:message code="registration.payment.moment" var="paymentMoment" />
	<display:column property="payment.moment" title="${paymentMoment}" sortable="true" format="{0,date,dd/MM/yyyy HH:mm}" />

	<spring:message code="registration.fee" var="feeHeader" />
	<display:column property="payment.fee" title="${feeHeader}" sortable="false" />

</display:table>


<div>
	<a href="registration/create.do"> <spring:message
			code="registration.create" />
	</a>
</div>
